# 修改版说明
大陆访问 github 太慢了, 只能移到大陆码云服务器而没有做 pull request  
基于原模板的 v1.7.1 基础修改  
原地址: https://github.com/meteorlxy/vuepress-theme-meteorlxy  

## 使用方法
跟原版一样, 除了在 config.js 配置的 theme 变成了 meteorlxy-wei
```js
// 使用的主题
theme: 'meteorlxy-wei',
```

## 改动列表
- 个人信息框中的 sns 社交部分, 添加码云 gitee

## 其他改动
- 原版的 eslint 添加 extends standard 之后我 lint 不了, 于是去掉了这个


# VuePress Blog Theme - Meteorlxy

[![](https://img.shields.io/circleci/project/github/meteorlxy/vuepress-theme-meteorlxy/master.svg?style=flat)](https://circleci.com/gh/meteorlxy/vuepress-theme-meteorlxy)
[![](https://img.shields.io/npm/v/vuepress-theme-meteorlxy.svg?style=flat)](https://www.npmjs.com/package/vuepress-theme-meteorlxy)
[![](https://img.shields.io/github/license/meteorlxy/vuepress-theme-meteorlxy.svg?style=flat)](https://github.com/meteorlxy/vuepress-theme-meteorlxy/blob/master/LICENSE)

:heart: Meteorlxy blog theme for [VuePress](https://vuepress.vuejs.org)

## Documentation

:book: [Live Demo and Docs](https://vuepress-theme-meteorlxy.meteorlxy.cn)

## LICENSE

[MIT](https://github.com/meteorlxy/vuepress-theme-meteorlxy/blob/master/LICENSE) &copy; [@meteorlxy](https://github.com/meteorlxy) & [Contributors](https://github.com/meteorlxy/vuepress-theme-meteorlxy/graphs/contributors)
